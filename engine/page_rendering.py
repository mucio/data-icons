from jinja2 import Environment, FileSystemLoader, Template
from pathlib import Path
from image import Image
from folder import Folder


# Page rendering
class PageRendering:
    def __init__(self, template_folder: str):
        self.template_folder = template_folder

    def get_template(self, template_file: str) -> Template:
        env = Environment(loader=FileSystemLoader(self.template_folder))
        template = env.get_template(template_file)

        return template

    def create_page(self, template_file, data, target_folder, file_name):
        template = self.get_template(template_file=template_file)

        Path(target_folder).mkdir(parents=True, exist_ok=True)
        template.stream(image=data, page=data).dump(f"{target_folder}/{file_name}")

    def render_image_page(self, image_name: str, image_path: str) -> None:
        target_folder = f"{image_path.replace('content/images', 'target')}"

        img = Image(path=f"{image_name}")

        self.create_page(
            template_file="image_page.html",
            data=img,
            target_folder=target_folder,
            file_name=f"{img.name}.html",
        )

    def render_folder_page(
        self, folder_name: str, folder_path: str, folder_content: dict
    ):
 
        try:
            description = Path(f"{folder_path}/{folder_name}/desc.html").read_text()
        except:
            description = None
        print(folder_content)

        folder = Folder(
            path=f"{folder_path}/{folder_name}",
            content=folder_content,
            description=description,
        )

        target_folder = f"{folder.path.replace('content/images', 'target')}"

        self.create_page(
            template_file="folder_page.html",
            data=folder,
            target_folder=target_folder,
            file_name="index.html",
        )
