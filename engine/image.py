from dataclasses import dataclass, field
from typing import List


@dataclass(order=True)
class Image:
    name: str
    filename: str
    path: str
    folder: str
    alt: str
    tags: List[str] = field(default_factory=list)

    def __init__(self, path):
        folder = "/".join(path.split("/")[:-1])
        filename = path.split("/")[-1]
        name = filename.split(".")[0]
        parts = name.split("_")

        self.path = path
        self.folder = folder
        self.filename = filename
        self.name = "_".join(name.split("_")[:2])
        self.alt = f"An image for {self.name}"

        # Add the remaining parts to the tags list
        if len(parts) > 2:
            self.tags = parts[2:]
        else:
            self.tags = []

    def __post_init__(self):
        self.sort_index = self.name
