from dataclasses import dataclass, field
from typing import List
from image import Image


@dataclass
class Folder:
    name: str
    folder: str
    path: str
    description: str
    subfolders: List[str] = field(default_factory=list)
    images: List[Image] = field(default_factory=list)

    def __init__(self, path, content, description):
        target_path = path.replace("src/images", "target")

        folder = "/".join(target_path.split("/")[:-1])
        name = target_path.split("/")[-1:][0]

        subfolders = [k for k, v in content.items() if v["type"] == "folder"]

        images = [
            Image(path=f"""./{k}""") for k, v in content.items() if v["type"] == "image"
        ]
        images = sorted(images)

        self.name = name
        self.folder = folder
        self.path = target_path
        self.description = description
        self.subfolders = subfolders
        self.images = images
