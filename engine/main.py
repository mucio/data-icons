from pathlib import Path
import shutil
from page_rendering import PageRendering as PR

HOME_FOLDER = Path(__file__).parent.parent.resolve().as_posix()
IMAGES_FOLDER = Path("./content")


# f"{HOME_FOLDER}/engine/templates"


# Folders management
def delete_target():
    path = Path(f"{HOME_FOLDER}/target")
    shutil.rmtree(path, ignore_errors=True)


def copy_images_to_target():
    # Create Path objects for the source and destination folders
    source_path = Path(f"{HOME_FOLDER}/content/images")
    destination_path = Path(f"{HOME_FOLDER}/target")

    # Copy the source folder and its contents to the destination folder
    shutil.copytree(source_path, destination_path)


def collect_folder_structure(folder_path):
    folder_structure = {}

    # Iterate over all items in the folder
    for item in folder_path.iterdir():
        # Check if item is a file
        if item.is_file():
            folder_structure[item.name] = {
                "type": "desc" if item.name == "desc.html" else "image",
                "path": f"{HOME_FOLDER}/{folder_path.as_posix()}",
                "tags": None,
            }
        # Check if item is a subfolder
        elif item.is_dir():
            folder_structure[item.name] = {
                "type": "folder",
                "content": collect_folder_structure(item),
                "path": f"{HOME_FOLDER}/{folder_path.as_posix()}",
                "tags": None,
            }
    return folder_structure


def process_folder_structure(folder_structure):
    render = PR(f"{HOME_FOLDER}/engine/templates")

    for key, value in folder_structure.items():
        if value["type"] == "image":
            render.render_image_page(image_name=key, image_path=value["path"])
        elif value["type"] == "folder":
            render.render_folder_page(
                folder_name=key,
                folder_path=value["path"],
                folder_content=value["content"],
            )
            process_folder_structure(value["content"])
        else:
            pass


delete_target()
copy_images_to_target()
folder_structure_dict = collect_folder_structure(IMAGES_FOLDER)

process_folder_structure(folder_structure_dict)
